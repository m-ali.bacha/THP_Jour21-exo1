Rails.application.routes.draw do
  resources :movies
  get '/', to: 'movies#index'

  # get '/salut/:name', to: 'pages#salut'
  #resources :index
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
