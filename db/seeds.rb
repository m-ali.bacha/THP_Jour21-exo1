# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
movie1=  Movie.create(title:"ce que je veut",release_year:2018,director:"nolan")
movie2=  Movie.create(title:"Batman Begins",release_year:2008,director:"nolan")
movie3=  Movie.create(title:"Iron Man",release_year:2018,director:"nolan")
movie4=  Movie.create(title:"bon courage correcteur",release_year:2018,director:"moi-meme")
movie5=  Movie.create(title:"james bond",release_year:2019,director:"trump")
